"""
Specificity of this version: 
    kernel computing has been split into 2 steps. 
    The "Values" kernel is computed only if non-zero values are found for each of the 3 other kernels.
    Otherwise, it takes 0 value.
"""
import datetime
import logging
import multiprocessing
import numpy as np
import os
import random
import pandas as pd
import shutil
import sys

from logging.handlers import RotatingFileHandler

import functools

@functools.total_ordering
class Lexicographic(object):
    def __init__(self, values=None, maximize=True):
        if values is None:
            values = []
        self.values = values
        try:
            iter(maximize)
        except TypeError:
            maximize = [maximize for v in values]
        self.maximize = maximize

    def __len__(self):
        return len(self.values)
    
    def __getitem__(self, key):
        return self.values[key]
        
    def __iter__(self):
        return iter(self.values)
        
    def __lt__(self, other):
        for v, o, m in zip(self.values, other.values, self.maximize):
            if m:
                if v < o:
                    return True
                elif v > o:
                    return False
            else:
                if v > o:
                    return True
                elif v < o:
                    return False
        return False

    def __eq__(self, other):
        return (self.values == other.values and self.maximize == other.maximize)

    def __str__(self):
        return str(self.values)
        
    def __repr__(self):
        return str(self.values)
    
    
    
def equi_transfo(x) :
    return ((x-0.5)**3-(-0.5)**3)*4
    


def initialize_logging(path: str, log_name: str = "", date: bool = True) -> logging.Logger :
    """
    Function that initializes the logger, opening one (DEBUG level) for a file and one (INFO level) for the screen printouts.
    """

    if date:
        log_name = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S") + "-" + log_name
    log_name = os.path.join(path, log_name + ".log")

    # create log folder if it does not exists
    if not os.path.isdir(path):
        os.mkdir(path)

    # remove old logger if it exists
    if os.path.exists(log_name):
        os.remove(log_name)

    # create an additional logger
    logger = logging.getLogger(log_name)

    # format log file
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter("[%(levelname)s %(asctime)s] %(message)s",
                                  "%Y-%m-%d %H:%M:%S")

    # the 'RotatingFileHandler' object implements a log file that is automatically limited in size
    fh = RotatingFileHandler(log_name,
                             mode='a',
                             maxBytes=100*1024*1024,
                             backupCount=2,
                             encoding=None,
                             delay=0)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    logger.info("Starting " + log_name + "!")

    return logger


def close_logging(logger: logging.Logger) :
    """
    Simple function that properly closes the logger, avoiding issues when the program ends.
    """

    for handler in logger.handlers:
        handler.close()
        logger.removeHandler(handler)

    return

def observer(population, num_generations, num_evaluations, args) :
    """
    The observer is a classic function for inspyred, that prints out information and/or saves individuals. However, it can be easily re-used by other
    evolutionary approaches.
    """
    logger = args["logger"]
    save_directory = args["save_directory"]
    overwrite_population_file = args["overwrite_population_file"]

    # first, a check to verify the type of library we are working with
    library_used = "unknown"
    if hasattr(population[0], "candidate") and hasattr(population[0], "fitness") :
        # we are using inspyred
        library_used = "inspyred"
    else :
        library_used = "cma-es"
    # TODO eventually, special cases for other libraries

    best_individual = best_fitness = None

    if library_used == "inspyred" :
        best_individual = population[0].candidate
        best_fitness = population[0].fitness

    # some output
    logger.info("Generation %d (%d evaluations), best individual fitness: %s" % (num_generations, num_evaluations, best_fitness))


    # save the whole population to file
    if overwrite_population_file :
        population_file_name = "%d-%s-generation-last.csv" % (args["random_seed"], args["population_file_name"])
    else :
        # create file name, with information on random seed and population
        population_file_name = "%d-%s-generation-%d.csv" % (args["random_seed"], args["population_file_name"], num_generations)
        population_file_name = os.path.join(save_directory, population_file_name)
    
    logger.debug("Saving population file to \"%s\"..." % population_file_name)

    # create dictionary
    dictionary_df_keys = ["generation", "fitness_value"] + ["gene_%d" % i for i in range(0, len(best_individual))]
    dictionary_df = { k : [] for k in dictionary_df_keys }

    # check the different cases
    # TODO there might be a special, custom way to conver the individual to text
    if library_used == "inspyred" :

        for individual in population :

            dictionary_df["generation"].append(num_generations)
            dictionary_df["fitness_value"].append(individual.fitness)
            for i in range(0, len(individual.candidate)) :
                dictionary_df["gene_%d" % i].append(individual.candidate[i])

    # conver dictionary to DataFrame, save as CSV
    df = pd.DataFrame.from_dict(dictionary_df)
    df.to_csv(population_file_name, index=False)

    return

def multi_process_evaluator(candidates, args) :
    """
    Wrapper function for the multi-process evaluation of the fitness. In this
    case, processes are better than threads, because we can more easily use
    chdir() and other functions that create a mess on threads.
    """
    logger = args["logger"]
    n_processes = args["n_threads"]
    folder_id = args["folder_id"]
    executable_name_brief = args["executable_name_brief"]
    executable_name_long = args["executable_name_long"]
    variable_names = args["variable_names"]
    scaling_factor = args["scaling_factor"]
    input_file_name = args["input_file_name"]
    output_file_name = args["output_file_name"]
    discr_steps = args["discr_steps"]
    gen = args["gen"]
    
    # create a pool of processes (slightly different from threads, they don't share memory)
    process_pool = multiprocessing.Pool(processes=n_processes)
    
    # prepare the pool, creating a list of tasks to be run
    results = []
    for i, c in enumerate(candidates) :
        local_arguments = (i, folder_id, executable_name_brief, executable_name_long, variable_names,
                           scaling_factor, input_file_name, output_file_name, logger, c, gen, discr_steps)
        results.append(process_pool.apply_async(fitness_function_process, args=local_arguments))
    
    
    # run the tasks! the results will appear in the same order as the arguments given in 'results'
    logger.info("Starting multi-process evaluation of %d individuals..." % len(candidates))
    fitness_values = [p.get() for p in results]
    
    # close the process pool
    process_pool.close()
    process_pool.terminate()
    process_pool.join()
    
    # update the base value of folder_id for the next generation
    args["folder_id"] += len(candidates)
    args["gen"]+=1
    
    return fitness_values



def fitness_function_process(index, folder_id, executable_name_brief, executable_name_long, variable_names,
                   scaling_factor, input_file_name, output_file_name, logger, individual, gen, discr_steps) :
    """
    Function designed to run as a process.
    """
    
    folder_name = "individual_%d" % (folder_id + index)
    os.makedirs(folder_name, exist_ok=True)
    
    # change the working directory (only affects this process)
    os.chdir(folder_name)
    
    
    # we write the individual to a text file
    individual_text = ""
    for i in range(0, len(variable_names)) :
        variable_min = scaling_factor[variable_names[i]][0]
        variable_max = scaling_factor[variable_names[i]][1]
        true_value = individual[i] * (variable_max - variable_min) + variable_min
        individual_text += variable_names[i] + " = " + str(true_value) + "\n"
    
    DX1 = round(discr_steps["DX1"] + 30 / (1 + np.exp(-0.1*(gen-70))))
    individual_text += "DX1" + " = " + str(DX1) + "\n"
    DX4 = round(discr_steps["DX4"] + 30 / (1 + np.exp(-0.1*(gen-70))))
    individual_text += "DX4" + " = " + str(DX4) + "\n"
    DX7 = round(discr_steps["DX7"] + 30 / (1 + np.exp(-0.1*(gen-70))))
    individual_text += "DX7" + " = " + str(DX7) + "\n"
    DT = round(discr_steps["DT"] + 9.5 / (1 + np.exp(-0.1*(gen-80))))
    individual_text += "DT" + " = " + str(DT) + "\n"
    
    with open(input_file_name, "w") as fp :
        fp.write(individual_text)
    
        
    ### Surrogate model: we begin with the first three kernel values
    
    # equi_sc_R = (1-individual[1]) + individual[3] + 1-individual[7] # plus la valeur est élevée, plus l'effort à fournir est grand (a priori)
    # equi_sc_A = individual[1] + 1-individual[2] + individual[5]
    # equi_sc_H = 1-individual[0] + individual[4] + 1-individual[6] + individual[8]
    
    equi_sc_R = (equi_transfo(1-individual[1])) + equi_transfo(individual[3]) + equi_transfo(1-individual[7]) # plus la valeur est élevée, plus l'effort à fournir est grand (a priori)
    equi_sc_A = equi_transfo(individual[1]) + equi_transfo(1-individual[2]) + equi_transfo(individual[5])
    equi_sc_H = equi_transfo(1-individual[0]) + equi_transfo(individual[4]) + equi_transfo(1-individual[6]) + equi_transfo(individual[8])
    
    equi_vect = [equi_sc_A,equi_sc_H,equi_sc_R]
    
    if max(equi_vect)-min(equi_vect) > 2:
        fitness_values = [0,0,0,0]
        with open(output_file_name, "w") as fp :
            fp.write("surf_table = [0.0,0.0,0.0]")

    elif individual[0] == 0:
        fitness_values = [0,0,0,0]
        with open(output_file_name, "w") as fp :
            fp.write("surf_table = [0.0,0.0,0.0]")
    
    else:
        # we need to copy the original executable into the folder
        shutil.copy("../" + executable_name_brief, executable_name_brief)    
        
        # we call the executable, using the text file as argument
        command_line = executable_name_brief + " > local_output.txt"
        os.system(command_line)
        
        # we collect the results from another text file
        lines = []
        with open(output_file_name, "r") as fp :
            lines = fp.readlines()
            
        # parsing TODO; error control
        line_index = 0
        while not lines[line_index].startswith("surf_table") :
            line_index += 1
            
        # this is quick and dirty, maybe a regex would be better
        tokens_1 = lines[line_index][:-2].split("[")
        tokens_2 = tokens_1[1].split(",")
        
        fitness_values = []
        for i in range(0, len(tokens_2)) :
            fitness_values.append(float(tokens_2[i]))
            
        
        if min(fitness_values) == 0 :
            fitness_values.append(0)
        
        else:
            # we need to copy the original executable into the folder
            shutil.copy("../" + executable_name_long, executable_name_long)    
            
            # we call the executable, using the text file as argument
            command_line = executable_name_long + " > local_output.txt"
            os.system(command_line)
            
            # we collect the results from another text file
            lines = []
            with open(output_file_name, "r") as fp :
                lines = fp.readlines()
                
            # parsing TODO; error control
            line_index = 0
            while not lines[line_index].startswith("surf_V") :
                line_index += 1
                
            # this is quick and dirty, maybe a regex would be better
            tokens_3 = lines[line_index].split("=")
            fitness_values.append(float(tokens_3[1]))
        
    
    # FITNESS
    
    
    #fitness_value = Lexicographic([min(fitness_values), sum(fitness_values)], maximize=True)
    #fitness_value = Lexicographic([fitness_values[-1], sum(fitness_values)], maximize=True)
    #fitness_value = Lexicographic([min(fitness_values), sum(fitness_values), fitness_values[-1]], maximize=True)
    
    # MIN - EQU - SUM etc.
    # fitness_value = Lexicographic([min(fitness_values), 1/(max(equi_vect)-min(equi_vect)), sum(fitness_values), 1/max(equi_vect), fitness_values[-1]], maximize=True)

    # MIN - KER V - EQU etc.
#    fitness_value = Lexicographic([min(fitness_values), fitness_values[-1], 1/(max(equi_vect)-min(equi_vect)), sum(fitness_values), 1/max(equi_vect)], maximize=True)

    # EQU - MIN - SUM etc.
#    fitness_value = Lexicographic([int(min(fitness_values)!=0), 1/(max(equi_vect)-min(equi_vect)), 1/max(equi_vect), min(fitness_values), sum(fitness_values), fitness_values[-1]], maximize=True)

    # KER V - EQU - MIN etc
    # fitness_value = Lexicographic([int(min(fitness_values)!=0), fitness_values[-1], 1/(max(equi_vect)-min(equi_vect)), 1/max(equi_vect), min(fitness_values), sum(fitness_values)], maximize=True)

    # KER V - MIN - EQU etc
#    fitness_value = Lexicographic([int(min(fitness_values)!=0), fitness_values[-1], min(fitness_values), 1/(max(equi_vect)-min(equi_vect)), 1/max(equi_vect), sum(fitness_values)], maximize=True)

    # MIN - EFF - EQU - SUM etc.
    fitness_value = Lexicographic([min(fitness_values), 1/max(equi_vect), 1/(max(equi_vect)-min(equi_vect)), sum(fitness_values), fitness_values[-1]], maximize=True)

    # before returning, change directory to the main directory
    os.chdir("..")

    return fitness_value


def main() :

    # there are a lot of moving parts inside an EA, so some modifications will still need to be performed by hand
    
    # a few hard-coded values, to be changed depending on the problem
    # unique name of the directory
    log_directory = "unique-name"
    save_directory = log_directory
    population_file_name = "population.csv"
    overwrite_population_file = False # the whole population is saved at every iteration, if True on the same file
    random_seeds = [42] # list of random seeds, because we might want to run the evolutionary algorithm in a loop 
    n_threads = 12 # TODO change number of threads when running on large servers
    
    scaling_factor = dict()
    scaling_factor["X4V"] = [0, 1]
    scaling_factor["Rmin"] = [0, 55000]
    scaling_factor["EAmax"] = [0, 150000]
    scaling_factor["ERmin"] = [0, 150000]
    scaling_factor["GHmin"] = [0, 150000]
    scaling_factor["Smin"] = [0, 1]
    scaling_factor["Tomax"] = [0, 11000]
    scaling_factor["Pmax"] = [5, 40]
    scaling_factor["Fmin"] = [0, 1000]
    
    discr_steps = dict()
    discr_steps["DX1"] = 11
    discr_steps["DX4"] = 11
    discr_steps["DX7"] = 11
    discr_steps["DT"] = 5

    # initialize logging, using a logger that smartly manages disk occupation
    logger = initialize_logging(log_directory)

    # start program
    logger.info("Hi, I am a program, starting now!")
    logger.debug(type(logger))
    
    # create the directory where the results will be saved
    if not os.path.exists(save_directory) :
        os.makedirs(save_directory)

    # start a series of experiments, for each random seed
    for random_seed in random_seeds :

        logger.info("Starting experiment with random seed %d..." % random_seed)
        random_seed = random_seed

        # initalization of ALL random number generators, to try and ensure repatability
        prng = random.Random(random_seed)
        np.random.seed(random_seed) # this might become deprecated, and creating a dedicated numpy pseudo-random number generator instance would be better 

        # TODO depending on the algorithm, we are going to import and call different stuff 
        ea_type = "inspyred"
        #ea_type = "cma-es"
        
        if ea_type == "inspyred" :

            # create an instance of EvolutionaryComputation (generic EA) and set up its parameters
            # define all parts of the evolutionary algorithm (mutation, selection, etc., including observer)
            import inspyred
            ea = inspyred.ec.EvolutionaryComputation(prng)
            ea.selector = inspyred.ec.selectors.tournament_selection 
            ea.variator = [inspyred.ec.variators.n_point_crossover, inspyred.ec.variators.gaussian_mutation]
            ea.replacer = inspyred.ec.replacers.plus_replacement
            ea.terminator = inspyred.ec.terminators.evaluation_termination
            ea.observer = observer
            ea.logger = logger

            # also create a generator function
            def generator(random, args) :
                return [ random.uniform(0, 1) for _ in range(args["n_dimensions"]) ]

            final_population = ea.evolve(
                                    generator=generator,
                                    #evaluator=multi_thread_evaluator,
                                    evaluator=multi_process_evaluator,
                                    pop_size=100,
                                    num_selected=200,
                                    maximize=True,
                                    bounder=inspyred.ec.Bounder(0, 1),
                                    max_evaluations=20000,

                                    # all items below this line go into the 'args' dictionary passed to each function
                                    logger = logger,
                                    n_dimensions = 9,
                                    n_threads = n_threads,
                                    population_file_name = population_file_name,
                                    random_seed = random_seed,
                                    save_directory = save_directory,
                                    overwrite_population_file = overwrite_population_file,
                                    
                                    # extra variables that we need for this application
                                    executable_name_brief = "main_ARH.exe",
                                    executable_name_long = "main_V.exe",
                                    input_file_name = "input.txt",
                                    output_file_name = "sizes_output.txt",
                                    folder_id = 0,
                                    variable_names = ["X4V", "Rmin", "EAmax", "ERmin", 
                                                      "GHmin", "Smin", "Tomax", "Pmax", "Fmin"],
                                    scaling_factor = scaling_factor,
                                    discr_steps = discr_steps,
                                    gen = 0
                                    )


    # TODO do something with the best individual
    best_individual = final_population[0]

    # close logger
    close_logging(logger)

    return

if __name__ == "__main__" :
    sys.exit( main() )

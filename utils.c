#include <math.h>

//*********************** Ensuite passer d'un point de la grille avec des indices compris entre 0 et D-1 à un point réel et vice versa *****************************************

//Fontion qui donne l'indice associé à une valeur réelle, les bornes et la précision D

int Indice(float x,float xmax, float xmin,int Dx){
	int ix;
	ix=(int)((x-xmin)*(Dx-1)/(xmax-xmin));
	return fmaxf(fminf(ix,Dx-1),0);
}


//Fonction qui donne la valeur  associéé à l'indice

float Valeur(int ix,float xmax, float xmin,int Dx){
	float x;
	x=xmin+(xmax-xmin)*(float)(ix)/(Dx-1);
	return x;
}


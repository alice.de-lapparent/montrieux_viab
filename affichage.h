#ifndef _AFFICHAGE_H_
#define _AFFICHAGE_H_


#include "organisation3.h"

#define PREFIXEFICHIER "GuarViab_Montrieux"
#define VERSION "2.2"

void InscriptionParametres(FILE *f);



void O3_InscriptionResultats(FILE *f, float surfaces_Agricole2D[DR][DE], float surfaces_Restaurant2D[DR][DE],float surfaces_Val[DR][DE], float surfaces_produit[DR][DE]);

void Affichage_vecteur(FILE *f,float xmax,float xmin,int dx);

void Affichage_Noyau_1D_x3(FILE *f,int Values[DX3]);

void Affichage_Frontiere_Noyau_2D_x2x1(FILE *f,int Values[DX2]);

void Affichage_Noyau_1D_x2(FILE *f,int Values[DX2]);

void Affichage_Frontiere_Noyau_2D_x3x1(FILE *f,int Values[DX3]);

void Affichage_Frontiere_Noyau_3D_x4x1x7(FILE *f, int DX4, int DX7,int Values[DX4][DX7]);

void Affichage_Frontiere_Noyau_2D_x5x6(FILE *f,int Values[DX5][DX6]);

void Affichage_Frontiere_Produit(FILE*f, int ValuesX2[DX2], int ValuesX3[DX3]);


void Affichage_Traj(FILE *f, float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX]);
void Affichage_Traj_i(FILE *f, float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX],int n);

//Sortie destinée à être copiée_collée dans le otebook python
void Affichage_pour_affichage_python(FILE *f,int DX1, int DX4,int DX7,int Values_O3_x3x1[DX3],int Values_O3_x2x1[DX2],int Values_O3_x4x1[DX4][DX7], int Values_x5x6[DX5][DX6]);

// Functions to save and load 3D kernel
void SaveKernel(FILE *f,int DX4,int DX7,int ValuesU[DX4][DX7]);

void LoadKernel(FILE *f,int DX4,int DX7,int ValuesU[DX4][DX7]);

void SaveTimes(FILE *f, float comp_times[DR][DE]);


//void Affichage_Surfaces(FILE *f, float X4V, float Rmin, float EAmax, float ERmin, float GHmin, float Smin, float Tomax, float Pmax, float Fmin, int Intx1x3frontiere[DX3], int Intx2frontiere[DX2], int Intx5x6frontiere[DX5][DX6], int Intx4frontiere[DX4][DX7]);


#endif

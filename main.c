#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include "organisation3.h"
#include "utils.h"
#include "dynamics.h"
#include "affichage.h"
#include "init.h"





int main(int argc, char *argv[]){
	int b,i,j,j1,j2,k;
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];

    int V_Intx1[DX1][DX7],Intx1frontiere[DX1];
	int Intx3[DX3],Intx2frontiere[DX2];
	int Intx2[DX2],Intx3frontiere[DX3];
	int R_Intx2[DX2],A_Intx3frontiere[DX3],H_Intx5x6frontiere[DX5][DX6];
	int Intx2x3frontiere[DX2][DX3], traj_u1[TMAX];
	float traj_x1[TMAX],traj_x2[TMAX], traj_x3[TMAX], traj_u2[TMAX], traj_u3[TMAX];
	clock_t start, end;
	double run_time;

    //printf("RMAX %f ",production_maximale());
	//printf("GMAX %f \n",cout_maximal());


	float X4V = 0.4;

	int exec_mode = 3;
	// 1 = calcul des noyaux pour un set d'engagements donne
	// 2 = calcul des surfaces des noyaux pour l'ensemble des sets d'engagements
	// 3 = engagements entres en input, volumes en output

	if (exec_mode == 1){

	// Commitments :
	float Rmin = 20000;
	float EAmax = 50000;
	float ERmin = 50000;
	float GHmin = 1000;
	float Smin = 0.3;
	float Tomax = 10000;
	float Pmax = 30;
	float Fmin = 50;



	FILE* sauvT = fopen("Exec_times.txt", "w");

	printf("Agriculture\n");
	start = clock();
	O3_CalculNoyauAgricole2D(A_Intx3frontiere,Rmin,EAmax,ERmin,Smin);
	end = clock();
	run_time = ((double)(end-start))/CLOCKS_PER_SEC;
	fprintf(sauvT,"Execution time Agri = %f\n", run_time);

	printf("Restaurant\n");
	start = clock();
	O3_CalculFrontiereNoyauRestaurant2D(R_Intx2,Rmin, ERmin,Pmax);
	end = clock();
	run_time = ((double)(end-start))/CLOCKS_PER_SEC;
	fprintf(sauvT,"Execution time Restau = %f\n", run_time);

	printf("Hotel\n");
	start = clock();
	CalculNoyauHotel2D(H_Intx5x6frontiere,GHmin,Tomax,Fmin,X4V);
	end = clock();
	run_time = ((double)(end-start))/CLOCKS_PER_SEC;
	fprintf(sauvT,"Execution time Hotel = %f\n", run_time);

	printf("Global\n");
	start = clock();
	O3_CalculFrontiereNoyauValues(V_Intx1,Smin,EAmax,ERmin,GHmin,Tomax, Fmin, Pmax, X4V);
	end = clock();
	run_time = ((double)(end-start))/CLOCKS_PER_SEC;
	fprintf(sauvT,"Execution time Values : %f \n",run_time);


	FILE *f;
	f = fopen("output.txt", "w");

    Affichage_pour_affichage_python(f,A_Intx3frontiere,R_Intx2, V_Intx1, H_Intx5x6frontiere);
//    fprintf(f, "O3_Frontiere_Produit_x2x3 =");
//    Affichage_Frontiere_Produit(f, R_Intx2, A_Intx3frontiere);


	fclose(f);
	fclose(sauvT);
}

else if (exec_mode == 2){


//// Noyaux organisation 3

	int Intx1x3frontiere[DX3];
	float surfaces_Agricole2D[DR][DE],surfaces_Restaurant2D[DR][DE],surfaces_Val2D[DR][DE],surfaces_produit[DR][DE], comp_times[DR][DE];
	float Rmin,EAmax,ERmin,Smin;


	float GHmin = 50000;
	float Tomax = 2000;
		float Pmax = 20;
	float Fmin = 100; // Je d�finis ici ces quelques engagements pour �viter les messages d'erreurs, mais id�alement il faudrait les inclure dans la suite


			for (j2=0;j2<DE;j2++){
                    EAmax = Valeur(j2,EMAX,0.0,DE);
            for (k=0;k<DE;k++){
                    Smin = Valeur(j,SMAX,0.0,DE);

	for (i=0;i<DR;i++){
		Rmin = Valeur(i,RMAX,0.0,DR);
//		printf("\nRmin %f \n",Rmin);
		for (j1=0;j1<DE;j1++){
                ERmin = Valeur(j1,EMAX,0.0,DE);

//			printf("ERmin %f ",ERmin);
//			printf("Noyaux3 ");
            start = clock();
			O3_CalculNoyauAgricole2D(Intx1x3frontiere,Rmin,EAmax,ERmin,Smin);
//			printf("Noyaux1x2 ");
			O3_CalculFrontiereNoyauRestaurant2D(Intx2frontiere,Rmin,ERmin,Fmin);
			O3_CalculFrontiereNoyauValues(V_Intx1,Smin,EAmax,ERmin,GHmin,Tomax,Fmin,Pmax,X4V);
            end = clock();
			surfaces_Agricole2D[i][j1] = O3_CalculSurfacenoyauAgricole2D(Intx1x3frontiere);
			surfaces_Restaurant2D[i][j1] = O3_CalculSurfacenoyauRestaurant2D(Intx2frontiere);
            surfaces_produit[i][j1] = O3_CalculSurfacenoyauProduit(Intx1x3frontiere, Intx2frontiere);
            surfaces_Val2D[i][j1] = O3_CalculSurfacenoyauVal3D(V_Intx1);
            run_time = ((double)(end-start))/CLOCKS_PER_SEC;
			comp_times[i][j1] = run_time;



		}
	}


	printf("\n VALUES\n");
	for (i=0;i<DE;i++){
		if (i==0) printf("[");
		else printf(",");
		for (j=0;j<DR;j++){
			if (j==0) printf("[");
			else printf(",");
			printf("%f",surfaces_Val2D[j][i]);
			if (j==DR-1) printf("]");
		}
		if (i==DE-1) printf("]");
	}
//    getchar();

	FILE* sauv = NULL ;
	sauv = fopen("ResultsO3.txt", "a");
	FILE* sauvT = fopen("Exec_timesO3.txt", "w");


	if (sauv != NULL){
//        InscriptionParametres(sauv);
        O3_InscriptionResultats(sauv, surfaces_Agricole2D, surfaces_Restaurant2D, surfaces_Val2D, surfaces_produit);
        fclose(sauv);
        SaveTimes(sauvT,comp_times);
        fclose(sauvT);
	}
	else {
        printf("WARNING : File saving failed !");
	}
}}

   return 0;
}
else if (exec_mode == 3){
        int skip_comp = 1 ; // 0 = calcul ; 1 = skip
//    	float X4V;
        	float Rmin;
	float EAmax;
	float ERmin;
	float GHmin;
	float Smin;
	float Tomax;
	float Pmax;
	float Fmin;
//printf("X4V = ");
//scanf("%f", &X4V);
//printf("Rmin = ");
//scanf("%f", &Rmin);
//printf("EAmax = ");
//scanf("%f", &EAmax);
//printf("ERmin = ");
//scanf("%f", &ERmin);
//printf("GHmin = ");
//scanf("%f", &GHmin);
//printf("Smin = ");
//scanf("%f", &Smin);
//printf("Tomax = ");
//scanf("%f", &Tomax);
//printf("Pmax = ");
//scanf("%f", &Pmax);
//printf("Fmin = ");
//scanf("%f", &Fmin);


FILE *f_input;
	f_input = fopen("input.txt", "r");

	if (f_input != NULL){

    X4V = ReadValue(f_input);
    Rmin = ReadValue(f_input);
	EAmax = ReadValue(f_input);
	ERmin = ReadValue(f_input);
	GHmin = ReadValue(f_input);
	Smin = ReadValue(f_input);
	Tomax = ReadValue(f_input);
	Pmax = ReadValue(f_input);
	Fmin = ReadValue(f_input);

	fclose(f_input);
}
else{
    printf("file error");
}

printf("Agriculture\n");
	O3_CalculNoyauAgricole2D(A_Intx3frontiere,Rmin,EAmax,ERmin,Smin);

	printf("Restaurant\n");
	O3_CalculFrontiereNoyauRestaurant2D(R_Intx2,Rmin, ERmin,Pmax);


	printf("Hotel\n");
	CalculNoyauHotel2D(H_Intx5x6frontiere,GHmin,Tomax,Fmin,X4V);


	printf("Global\n");
	FILE *f_ker;

	if (skip_comp == 0){
    O3_CalculFrontiereNoyauValues(V_Intx1,Smin,EAmax,ERmin,GHmin,Tomax, Fmin, Pmax, X4V);
	f_ker = fopen("kernel3D.dat", "wb");
	SaveKernel(f_ker, V_Intx1);
	fclose(f_ker);}

	else{
	f_ker = fopen("kernel3D.dat", "rb");
	LoadKernel(f_ker, V_Intx1);
    fclose(f_ker);
	}


	FILE *f;
	f = fopen("output.txt", "w");

    Affichage_pour_affichage_python(f,A_Intx3frontiere,R_Intx2, V_Intx1, H_Intx5x6frontiere);

	fclose(f);

	printf("Volume agricole = %f \n", O3_CalculSurfacenoyauAgricole2D(A_Intx3frontiere));
printf("Volume restaurant = %f \n", O3_CalculSurfacenoyauRestaurant2D(R_Intx2));
printf("Volume hotel = %f \n", O3_CalculSurfacenoyauHotel2D(H_Intx5x6frontiere));
printf("Volume valeurs = %f \n", O3_CalculSurfacenoyauVal3D(V_Intx1));


	f = fopen("sizes_output.txt", "w");

    Affichage_Surfaces(f, X4V, Rmin, EAmax, ERmin, GHmin, Smin, Tomax, Pmax, Fmin, A_Intx3frontiere, R_Intx2, H_Intx5x6frontiere, V_Intx1);

	fclose(f);


}
else{
        printf("exec_mode invalide");
}
}

//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "organisation3.h"
#include "dynamics.h"
#include "utils.h"


//************************Plusieurs facons d'envisager la s�paration des univers restaurant et activit� agricole

//Organisation : La tr�sorerie est g�r�e uniquement en commun. Chaque activit� s'engage sur ses d�penses ou gains. La production agricole est utilis�e directement par le restaurant, sans facturation.

// Global:
//- 3 �tats : x_1 (tr�sorerie), x_4 (qualit� environnementale) et x_7 (valeur sociale)
//- 0 contr�les
//- 5 tyche : GR (gains restau), GH (gains hotel), D (d�penses agri), S (qualit� des sols), T (fr�quentation touristique), F (r�sidents invit�s), P (prix du menu)
//

//Agriculture:
//- 1 �tat : x_3 (qualit� des sols)
//- 2 contr�les : u_1 (assolement) et u_2 (surface)
//- 2 engagements : sur la production Rmin et sur ses d�penses Emax
//
//Restaurant :
//- 1 �tat : x_2 (attractivit�)
//- 1 contr�le : u_3 (prix du repas)
//- 1 tyche : R = production agricole (R >= Rmin)
//- 2 engagements : sur les gains Emin et sur les prix appliqu�s Pmax

// Hotel :
//- 2 �tats : x_5 (touristes) et x_6 (infrastructures)
//- 3 contr�les : u_4 (comp�titivit�), u_5 (r�investissement) et u_6 (prop invit�s)
//- 1 tyche : E (qualit� environnementale)
//- 2 engagements : sur les gains GH et le nombre d'invit�s F
//



//****************** Agriculteur *************************

//La dynamique de x3 :
float fx3next(float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float x3min,float x3max){
	float x3next;
	x3next = BISQ_evol(x3,u1,Assolements,u2,x3min,x3max);
	return x3next;
}

// La contrainte verifie que x3 dans l'ensemble de contraintes (ou autrement dit de satisfaction)*****************************************

int IsinConstraintSetAgricole2D(float x3, float Smin){
	int b=1;
	if (x3 < Smin) b=0;
	return b;
}


// fonction qui verifie si la paire de controles (u1,u2) est admissible lorsque l'�tat du syst�me est (x1,x3) �tant donn� l'engagement pris *****************************************

int IsinAdmissibleControlSetAgricole2D(float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2, float Rmin, float EAmax, float Smin){
	int b=1;
	if (R_nS(x3,u1,Assolements,u2)<Rmin) b = 0;
	else if ((Cout(u1,Assolements,u2))>EAmax) b=0;
//	else if (O3_fx3next(x3, u1, Assolements, u2, X3MIN, X3MAX)<Smin) b=0;
	return b;
}



void O3_CalculNoyauAgricole2D(int Intx1x3frontiere[DX3],float Rmin,float EAmax, float ERmin, float Smin){
	float x3min,x3max, u2min,u2max, x3,x3next,u2;
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
	int change,passage,i,b,ii,jj,u1,intx3next;


//Initialisation des bornes d'exploration
	x3min = fx3min();
	x3max= fx3max();
	b = CreateAssolements(Assolements);
	u2min = fu2min();
	u2max = fu2max();

// Initialisation de la variable qui indique s'il y aeu un changement
	change=1;
	passage = 1;

//Initialisation des tableaux qui donnent la valeur de la fronti�re du noyau en tant que H = f(ci) en float et en int (valeur de l'indice correspondant � Hmin �tant 0)

	for(i=0;i<DX3;i++) {
		x3= Valeur(i,x3max,x3min,DX3);
		if (IsinConstraintSetAgricole2D(x3,Smin)) Intx1x3frontiere[i]=1;
		else Intx1x3frontiere[i]=0;
	}

//Calcul
	while(change>0){
		//printf("change %d\n",change);
		change=0;
		//printf("passage %d\n",passage);
		passage = passage + 1;
		for(i=0;i<DX3;i++){
			if (Intx1x3frontiere[i] == 1){
				x3= Valeur(i,x3max,x3min,DX3);
				//printf("i x3  %d %f ",i,x3);
				if (IsinConstraintSetAgricole2D(x3,Smin)){
//					printf("IsinConstraintSet\n");
					b = 0;
					for(ii=0;ii<NB_ASSOLEMENTS;ii++) {
						u1 = ii;
						//printf("ii u1  %d %d\n",ii,u1);
						for(jj=0;jj<DU2;jj++) {
							u2 = Valeur(jj,u2max,u2min,DU2);
							//printf("jj u2  %d %f\n",jj,u2);
//							printf("Cout %f",Cout(u1,Assolements,u2));
//							getchar();
							if (IsinAdmissibleControlSetAgricole2D(x3,u1,Assolements,u2,Rmin,EAmax,Smin)){
								//printf("IsinAdmissibleControlSet\n");
								x3next = fx3next(x3,u1,Assolements,u2,x3min,x3max);
//								printf("i x3next  %d %f ",i,x3next);
								if (IsinConstraintSetAgricole2D(x3next,Smin)){
									intx3next = Indice(x3next,x3max, x3min,DX3);

									if (Intx1x3frontiere[intx3next]==1){
										b = 1;
										break;
									}
								}
							}
						}
						if (b ==1){
							break;
						}
					}
					if (b == 0){
						Intx1x3frontiere[i] = 0;
						//printf("intfrontiere %d\n",IntHfrontiere[i]);
						change = change + 1;
						//printf("change %d\n",change);
						//getchar();
					}
				}
				else 	Intx1x3frontiere[i] = 0;

			}
		}
		//printf("change total %d\n",change);
	}
/*
	printf("noyau\n");
	for(i=0;i<DX3;i++){
		printf("%d,",Intx1x3frontiere[i]);
	}
	printf("\n");
*/
}





// Calcul du noyau de viabilit� garanti du restaurant 1 dimension d'�tat x2, 1 contr�le u3, un tyche R et pas de contrainte mais un engagement.

// La contrainte verifie que (x1,x2) dans l'esnemble de contraintes (ou autrement dit de satisfaction)*****************************************

int IsinConstraintSetREstaurant2D(float x2){
	int b=1;
	return b;
}


int IsinAdmissibleControlSetRestaurant2D(float x2,float u3, float R, float Emin, float Pmax){
	int b=1;
	float c;
	c = Capital_evolutionR(x2,u3,R);
    if (c<Emin) b = 0;
    if (u3 > Pmax) b = 0 ;

	return b;
}

//Dynamique de x2

float fx2next(float x2,float u3,float Rn_x3_u1){
	float x2next;
	x2next = Attractiveness_evolution(x2,u3,Rn_x3_u1);
	return x2next;
}

//Calcul de la fronti�re inf�rieure du noyau de viabilit� garanti comme tableau de valeurs Intx2frontiere[DX2] qui associe � des valeurs de l'attractivit�  allant de X2MIN � X2MAX l'indice de la valeur minimale du capital x1 appartenant au noyau de viabilit� garanti


void O3_CalculFrontiereNoyauRestaurant2D(int Intx1frontiere[DX2],float Rmin,float Emin, float Pmax){
	float x2min,x2max,u3min,u3max,x2,u3,x2next,R;
	int change,passage,i,b,ii,iii,intx2next;


//Initialisation des bornes d'exploration
	x2min = fx2min();
	x2max= fx2max();
	u3min = fu3min();
	u3max = fu3max();

// Initialisation de la variable qui indique s'il y aeu un changement
	change=1;
	passage = 1;

//Initialisation des tableaux qui donnent la valeur de la fronti�re du noyau en tant que H = f(ci) en float et en int (valeur de l'indice correspondant � Hmin �tant 0)

	for(i=0;i<DX2;i++) {
		x2= Valeur(i,x2max,x2min,DX2);
		if (IsinConstraintSetREstaurant2D(x2)) Intx1frontiere[i]=1;
		else Intx1frontiere[i]=0;
	}

//Calcul
	while(change>0){
		//printf("change %d\n",change);
		change=0;
		//printf("passage R %d\n",passage);
		passage = passage + 1;
		for(i=0;i<DX2;i++){
			if (Intx1frontiere[i] == 1){
				x2= Valeur(i,x2max,x2min,DX2);
				b = 0;
				//printf("i x3  %d %f ",i,x3);
				if (IsinConstraintSetREstaurant2D(x2)){
//					printf("IsinConstraintSet\n");

					for(ii=0;ii<DU3;ii++) {
						u3 = Valeur(ii,u3max,u3min,DU3);
						//printf("ii u1  %d %d\n",ii,u1);
                        b = 1;
                        for(iii=0;iii<DR;iii++) {
                            R = Valeur(iii,RMAX,Rmin,DR);
                            //printf("iii Rn  %d %f\n",iii,Rn_x3_u1);
                            x2next = fx2next(x2,u3,R);
//                            printf("x2next %f \n",x2next);
                            //E=GainRepas(x2,u3)-CoutAchatFoodExterieur(x2,Rn_x3_u1)-FIXED_COSTSR;
                                if (IsinAdmissibleControlSetRestaurant2D(x2,u3,R,Emin,Pmax)){
								//printf("IsinAdmissibleControlSet\n");
                                    if (IsinConstraintSetREstaurant2D(x2next)){
                                        intx2next = Indice(x2next,x2max, x2min,DX2);
//									getchar();
                                        if (Intx1frontiere[intx2next]==0){
                                            b = 0; // viable
                                            break;
                                        }
                                    }
                                    else{
                                        b = 0;
                                        break;
                                    }
                                }
                                else{
                                        b = 0;
                                        break;
                                    }
                        }
                        if (b ==1){
                            break;
                        }
					}
					if (b == 0){
						Intx1frontiere[i] = 0;
						//printf("intfrontiere %d\n",IntHfrontiere[i]);
						change = change + 1;
						//printf("change %d\n",change);
						//getchar();
					}
                    }
				else 	Intx1frontiere[i] = 0;

			}
		}
		//printf("change total %d\n",change);
	}


/*	printf("frontiere\n");
	for(i=0;i<DX2;i++){
		printf("%d,",Intx1frontiere[i]);
	}
	printf("\n");
*/
}



//*************** HOTEL ********************


int IsinConstraintSetHotel2D(float x5, float x6, float Tomax){
	int b=0;
	if (x5<=Tomax) b = 1; // viable
	return b;
}


// fonction qui verifie si la paire de controles (u1,u2) est admissible lorsque l'�tat du syst�me est (x1,x3) �tant donn� l'engagement pris *****************************************

int IsinAdmissibleControlSetHotel2D(float x5, float x6,float u4, float u5, float u6, float GHmin, float Fmin){
	int b = 1;
	if (GainH(x5,x6,u4,u5,u6)<GHmin) b = 0;
	if (u6 * x5 < Fmin) b = 0;
	//printf("cout %f production %f\n",Cout(u1,Assolements,u2),R_nS(x3,u1,Assolements,u2));
	return b;
}

float fx5next(float x5,float x6, float u4, float u5, float E){
	float x5next;
	x5next = Tourists_evol(x5,x6,u4,E);
	return fmaxf(x5next,0);
}

float fx6next(float x5, float x6, float u4, float u5){
	float x6next;
	x6next = Infrastructures_evol(x5,x6,u4,u5);
	return x6next;
}

void CalculNoyauHotel2D(int Intx5x6frontiere[DX5][DX6],float GHmin,float Tomax, float Fmin, float X4V){
	float x5min,x5max,x5, x6min,x6max, u4min,u4max, x6,x5next,x6next,u4,u5,E,u5min,u5max,u6;
	int change,passage,i,j,b,ii,jj,jjj,iii,intx6next,intx5next;


//Initialisation des bornes d'exploration
	x5min = X5MIN;
	x5max= X5MAX;
	x6min = X6MIN;
	x6max= X6MAX;

	u4min = U4MIN;
	u4max = U4MAX;
    u5min = U5MIN;
	u5max = U5MAX;

// Initialisation de la variable qui indique s'il y aeu un changement
	change=1;
	passage = 1;

//Initialisation des tableaux qui donnent la valeur de la fronti�re du noyau en tant que H = f(ci) en float et en int (valeur de l'indice correspondant � Hmin �tant 0)

j = Indice(Tomax,x5max,x5min,DX5);
	for(i=0;i<DX6;i++) {
            for(ii=0;ii<DX5;ii++) {
		if (Valeur(ii,x5max,x5min,DX5)<=Tomax) Intx5x6frontiere[ii][i]=1;
    else Intx5x6frontiere[ii][i]=0;
	}}

//Calcul
	while(change>0){
		//printf("change %d\n",change);
		change=0;
//		printf("passage %d\n",passage);
		passage = passage + 1;
		for(i=0;i<DX6;i++){
            x6= Valeur(i,x6max,x6min,DX6);
//			printf("i x6  %d %f\n",i,x6);
			for(j=0;j<DX5;j++){
                    if (Intx5x6frontiere[j][i]==1){
//                    if (i==DX3-1) {printf("passage %d jmin %d j %d\n", passage,Intx1x3frontiere[i],j); getchar();}
				b = 0;
				x5 = Valeur(j,x5max,x5min,DX5);
//				printf("j x5 %d %f\n",j,x5);
				if (IsinConstraintSetHotel2D(x5,x6,Tomax)){
                    for(ii=0;ii<DU4;ii++) {
						u4 = Valeur(ii,u4max,u4min,DU4);
//						printf("ii u4  %d %f\n",ii,u4);
					//printf("IsinConstraintSet\n");
                        for(jj=0;jj<DU5;jj++) {
                            u5 = Valeur(jj,u5max,u5min,DU5);
//                            printf("jj u5  %d %f\n",jj,u5);
                            for (iii=0;iii<DU6;iii++){
                                u6 = Valeur(iii,U6MAX,U6MIN,DU6);

                            if (IsinAdmissibleControlSetHotel2D(x5,x6, u4, u5,u6,GHmin,Fmin)){
                                //printf("IsinAdmissibleControlSet\n");
                                b = 1;
                                x6next = fx6next(x5,x6,u4,u5);
//                                printf("x6next %f \n",x6next);
                                for(jjj=0;jjj<DE;jjj++) {
                                    E = Valeur(jjj,EMAX,X4V,DE);
//                                    printf("jjj %d E %f\n",jjj,E);
                                    x5next = fx5next(x5,x6,u4,u5,E);
//                                    printf("x5next %f \n",x5next);
                                    if (IsinConstraintSetHotel2D(x5next,x6next,Tomax)){
                                            intx5next = Indice(x5next,x5max, x5min,DX5);
//                                            printf("ix5next %d \n",intx5next);
                                        intx6next = Indice(x6next,x6max, x6min,DX6);
                                        //getchar();
                                        if (Intx5x6frontiere[intx5next][intx6next] == 0){
                                            b = 0;
                                            break;
                                        }
                                    }
                                    else{
                                        b = 0;
                                        break;
                                    }
                                }

                            }
                        if(b == 1){
                            //getchar();
                            break;
                        }
                    }
                    if(b == 1){
                            //getchar();
                            break;
                        }
                    }
                    if(b == 1){
                                //getchar();
                                break;
                            }
                        }
				}
				if (b == 0){
					Intx5x6frontiere[j][i] = 0;
					change = change + 1;
//					printf("intfrontiere %d\n",Intx5x6frontiere[j][i]);
//					getchar();
				}
			}
			}
		}
	}
}



// ********* Calcul du noyau de viabilit� garanti du module Global : 2 �tats, pas de controles, 5 tyche et pas d'engagements

//Dynamiques de x1 et x4

float O3_fx1next(float x1,float Er,float Ea, float Eh){
	float x1next;
	x1next = x1 - Ea + Er + Eh;
	return x1next;
}

float O3_fx4next(float x4,float S, float Eh, float Th){
	float x4next;
	x4next = Env_evol(x4,Eh,Th,S);
	return x4next;
}

float fx7next(float x7, float T, float F, float P){
    return fminf(fmaxf(Socio_evol(x7,T,F,P),0),1);
    }


// La contrainte verifie que (x1,x2) dans l'esnemble de contraintes (ou autrement dit de satisfaction)*****************************************

int O3_IsinConstraintSetValues(float x1,float x4, float x7, float X4V){
	int b=1;
	if (x1<X1V) b = 0;
	else if (x4<X4V) b=0 ;
	else if (x7<X7V) b = 0;
	return b;
}


int O3_IsinAdmissibleControlSetValues(float x1,float x4, float x7, float Er, float Ea, float S, float Eh, float Th, float F, float P, float X4V){

	return O3_IsinConstraintSetValues(O3_fx1next(x1,Er,Ea,Eh),O3_fx4next(x4,S, Eh, Th), fx7next(x7,Th,F,P), X4V);
}



//Calcul de la fronti�re inf�rieure du noyau de viabilit� garanti comme tableau de valeurs Intx2frontiere[DX2] qui associe � des valeurs de l'attractivit�  allant de X2MIN � X2MAX l'indice de la valeur minimale du capital x1 appartenant au noyau de viabilit� garanti


void O3_CalculFrontiereNoyauValues(int DX1, int DX4, int DX7, int DT, int Intx1frontiere[DX4][DX7],float Smin,float EAmax,float ERmin, float GHmin, float Tomax, float Fmin, float Pmax, float X4V){
	float x1min,x1max,x4min,x4max,x1,x4,x7,x1next,x4next,x7next,Er,Ea,S,Eh,Th,F,P;
	int change,passage,i,j,b,ii,iii,jj,jjj,jjjj,jjjjj,j6,j7,intx4next,intx7next,c;
	int iTomax,iSmin,iEAmax,iERmin,iGHmin,iFmin,iPmax;
	int DG , DTo , DS , DF , DP;
	clock_t start;
	double run_time;

	DG = DTo = DS = DF = DP = DT;

	start = clock();

//printf("%f", GHmin);

//Initialisation des bornes d'exploration
	x1min = fx1min();
	x1max= fx1max();
	x4min = X4MIN;
	x4max = X4MAX;

	iTomax = Indice(Tomax,ToMAX,ToMIN,DTo);
	iSmin = Indice(Smin,SMAX,SMIN,DS);
	iEAmax = Indice(EAmax,GMAX,GMIN,DG);
	iERmin = Indice(ERmin,GMAX,GMIN,DG);
	iGHmin = Indice(GHmin,GMAX,GMIN,DG);
	iFmin = Indice(Fmin,FMAX,FMIN,DF);
	iPmax = Indice(Pmax,U3MAX,U3MIN,DP);

//printf("%d",iGHmin);

// Initialisation de la variable qui indique s'il y aeu un changement
	change=1;
	passage = 1;

//Initialisation des tableaux qui donnent la valeur de la fronti�re du noyau en tant que H = f(ci) en float et en int (valeur de l'indice correspondant � Hmin �tant 0)

	for(i=0;i<DX4;i++) {
		x4= Valeur(i,x4max,x4min,DX4);
		for(j=0;j<DX7;j++){
                x7= Valeur(j,X7MAX,X7MIN,DX7);
		if (O3_IsinConstraintSetValues(X1V,x4,x7,X4V)) Intx1frontiere[i][j]=0;
		else Intx1frontiere[i][j]=DX1;
//		printf("x3 %f intfrontiere %d viab %d\n",x3,Intx1x3frontiere[i],O2_IsinConstraintSetAgricole2D(X1V,x3));
	}}


c=0;

//Calcul
	while(change>0){
//		printf("change %d\n",change);
//printf("[");
//	for (i=0;i<DX4;i++){
//		printf("%d",Intx1frontiere[i]);
//		if (i < DX4-1) printf(",");
//	}
//    printf("]\n");
		change=0;
		printf("passage V %d\n",passage);
		passage = passage + 1;
		for(i=0;i<DX4;i++){
            x4= Valeur(i,x4max,x4min,DX4);
            printf("i x4  %d %f\n",i,x4);
            for(iii=0;iii<DX7;iii++){
            x7= Valeur(iii,X7MAX,X7MIN,DX7);
//			printf("i x7  %d %f\n",iii,x7);
			for(ii=Intx1frontiere[i][iii];ii<DX1;ii++){
//                    if (i==DX4-1) {printf("passage %d jmin %d j %d\n", passage,Intx1frontiere[i],ii); getchar();}
				b = 0;
				x1 = Valeur(ii,x1max,x1min,DX1);
				//printf("j x1 %d %f\n",j,x1);
				if (O3_IsinConstraintSetValues(x1,x4,x7,X4V)){
//                        printf("True");
                    b = 1;
                    for (jjj=iGHmin;jjj<DG;jjj++){
                            Eh = Valeur(jjj,GMAX,GMIN,DG);
//                    printf("iEh %d, Eh %f\n", jjj, Eh);
                            for (j7=iFmin;j7<DF;j7++){
                                            F = Valeur(j7,FMAX,FMIN,DF);
//                        for(jjjj=Indice(F,ToMAX,ToMIN,DTo);jjjj<iTomax;jjjj++){
                                for(jjjj=iTomax;jjjj>Indice(F,ToMAX,ToMIN,DTo);jjjj--){
                            Th = Valeur(jjjj,ToMAX,ToMIN,DTo);

                                for(j=iSmin;j<DS;j++) {
                                    S = Valeur(j,SMAX,SMIN,DS);

                                x4next = O3_fx4next(x4,S,Eh,Th);
//                                c=c+1;
//                                printf("x4 %f,%f,%f\n", x4, S, x4next);
//                                for(jj=0;jj<iEAmax;jj++) {
                                       for(jj=iEAmax;jj>0;jj--) {
                                        Ea = Valeur(jj,GMAX,GMIN,DG);
                                    for(jjjjj=iERmin;jjjjj<DG;jjjjj++) {
                                    Er = Valeur(jjjjj,GMAX,GMIN,DG);
                                    x1next = O3_fx1next(x1,Er, Ea,Eh);
//                                    c=c+1;

//                                            for (j6=0;j6<iPmax;j6++){
                                            for (j6=iPmax;j6>0;j6--){
                                                P = Valeur(j6,U3MAX,U3MIN,DP);
                                                x7next = fx7next(x7,Th,F,P);
//                                                c=c+1;
//                                                printf("x7 %f, %f\n", x7, x7next);
                                    if (O3_IsinConstraintSetValues(x1next,x4next,x7next, X4V)){
                                        intx4next = Indice(x4next,x4max, x4min,DX4);
                                        intx7next = Indice(x7next,X7MAX, X7MIN,DX7);
                                        if (x1next < Valeur(Intx1frontiere[intx4next][intx7next],x1max,x1min,DX1) || Intx1frontiere[intx4next][intx7next] == DX1){
                                            b = 0;
//                                            printf("[");
//	for (i=0;i<DX4;i++){
//		printf("%d",Intx1frontiere[i]);
//		if (i < DX4-1) printf(",");
//	}
//    printf("]\n");
//                                            printf("%f,%d,%f\n", x1next,intx4next,Valeur(Intx1frontiere[intx4next],x1max,x1min,DX1));

                                            break;
                                        }

                                    }
                                    else{
                                        b = 0;
                                        break;
                                    }
                                }

                        if(b == 0){
                            break;
                        }
                                }
                                if(b == 0){
                            break;
                        }
                                }
                                if(b == 0){
                            break;
                        }
                                }
                    if(b == 0){
                                break;
                            }
                                }
                if(b == 0){
                            break;
                        }
                        }
            if(b == 0){
                            break;
                        }
//                        printf("x1 %f,x1next %f, b %d\n", x1, x1next,b);
//                                    printf("Ea %f, Er %f, Eh %f\n", Ea, Er, Eh);
//                                    getchar();
                    }

				if (b == 1){
					Intx1frontiere[i][iii] = ii;
//					printf("(%d,%d)",i,ii);

					break;
				}
				else {
					change = change + 1;
				}
			}}
			if (b == 0){
				Intx1frontiere[i][iii] = DX1;

			}

            }
//            printf("c = %d \n",c);
	}
//	printf("[");
//	for (i=0;i<DX4;i++){
//		printf("%d",Intx1frontiere[i]);
//		if (i < DX4-1) printf(",");
//	}
//    printf("]\n");

run_time = ((double)(clock()-start))/CLOCKS_PER_SEC;
if (run_time > 25000) {
    break;
}

}
	}


//Calcul du pourcentage d'occupation du noyau
float O3_CalculSurfacenoyauAgricole2D(int Intx1x3frontiere[DX3]){
	float surface;
	int i;
	surface = 0;
	for(i=0;i<DX3;i++) {
        surface=surface+(float)(Intx1x3frontiere[i]);
	}
	surface = surface/(DX3);
	return surface;
}

float O3_CalculSurfacenoyauRestaurant2D(int Intx2frontiere[DX2]){
	float surface;
	int i;
	surface = 0;
	for(i=0;i<DX2;i++) {
		surface=surface+(float)(Intx2frontiere[i]);
	}
	surface = surface/(DX2);
	return surface;
}

float O3_CalculSurfacenoyauHotel2D(int Intx5x6frontiere[DX5][DX6]){
	float surface;
	int i,j;
	surface = 0;
	for(i=0;i<DX5;i++) {
            for(j=0;j<DX6;j++) {
		surface=surface+(float)(Intx5x6frontiere[i][j]);
	}}
	surface = surface/(DX5*DX6);
	return surface;
}

float O3_CalculSurfacenoyauVal3D(int DX1, int DX4, int DX7, int Intx4frontiere[DX4][DX7]){
	float surface;
	int i,j;
	surface = 0;
	for(i=0;i<DX4;i++) {
            for(j=0;j<DX7;j++) {
		surface=surface+(float)(DX1)-(float)(Intx4frontiere[i][j]);
	}}
	surface = surface/(DX1*DX4*DX7);
	return surface;
}

//float O3_CalculSurfacenoyauProduit(int Intx1x3frontiere[DX3], int Intx2frontiere[DX2]){
//	float surface;
//	int i, j, k;
//	surface = 0;
//	for(i=0;i<DX2;i++) {
//            for(j=0;j<DX3;j++) {
//                    k = Indice(Valeur(Intx2frontiere[i],X1MAX,X1MIN,DX1)+Valeur(Intx1x3frontiere[j],X1MAX,X1MIN,DX1),X1MAX,X1MIN,DX1);
//		surface=surface+(float)(DX1)- (float)k;
//	}}
//	surface = surface/(DX1*DX2*DX3);
//	return surface;
//}




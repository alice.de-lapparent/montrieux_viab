#ifndef _UTILS_H_
#define _UTILS_H_

//Fontion qui donne l'indice associé à une valeur réelle, les bornes et la précision D

int Indice(float x,float xmax, float xmin,int Dx);


//Fonction qui donne la valeur  associéé à l'indice

float Valeur(int ix,float xmax, float xmin,int Dx);

#endif

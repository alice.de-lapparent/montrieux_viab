#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "init.h"

const char *  POSSIBLE_CROPS_NAMES[NB_POSSIBLE_CROPS] = {"Eggplant","Carrot","Cabbage","Cucumber","Squash","Greenbean","Lettuce","Melon","Tomato"};
const float POSSIBLE_CROPS_PARAM[NB_POSSIBLE_CROPS][18] = {{2.5,9.2,0.8, 0.2 ,0.12, 0,0,0,175,269,240,548,502,501,445,334,155,0},{2, 0.3,0.65, 0.14 ,0.07, 0,0,0,0,0,0,27,10,135,161,13,1,212},{1.3, 0.8,0.5, 0.21, 0.15,0,0,0,0,0,0,23,50,22,16,97,0,0},{2, 5.3,0.8, 0.28, 0.15 ,0,0,0,178,419,848,918,70,0,0,0,0,0},{3, 0.4,0.7, 0.17, 0.099, 0,0,0,0,16,40,4,5,5,115,0,0,0},{1.5, 5.4,0.6, 0.09 ,0.01, 0.002,0,0,457,19,55,2075,2044,156,0,0,0,0},{2.16, 1.7,0.6, 0.18, 0.12, 0,2,16,185,24,664,0,0,0,1,0,0,0},{2, 1.1,0.7, 0.17, 0.1, 0,0,4,11,145,27,17,255,35,4,0,0,0},{4 ,5.2,0.8, 0.2, 0.13, 0.002,0,26,143,296,557,705,586,411,235,326,0,0}};


float ReadValue(FILE *f){
    char chaine[100] = "";
    char *valstr;
    float valnum ;

    fgets(chaine, 100, f);


    if (chaine != NULL)
    {
        valstr = strchr(chaine,'=')+1;
//        printf("%s", valstr);
        if (valstr != NULL) // Si on a trouvé quelque chose
    {
        valnum = strtod(valstr,NULL);
//        printf("%f", valnum);
        return valnum;
    }
    else
    {
        printf("fail1");
        return 0;
    }}
    else
    {
        printf("fail2");
        return 0;
    }
}




//*************************D abord les bornes de toutes les variables*********servira peut etre plus tard si on a envie de les changer ....
//4 Fonctions qui "calculent" les bornes du domaine considéré pour les valeurs des 3 états : x1, x2 et x3

float fx1max(){
	float x1max;
	x1max =  X1MAX;
	return x1max;
}

float fx1min(){
	float x1min;
	x1min = X1MIN;
	return x1min;
}

float fx2max(){
	float x2max;
	x2max =  X2MAX;
	return x2max;
}

float fx2min(){
	float x2min;
	x2min =  X2MIN;
	return x2min;
}

float fx3max(){
	float x3max;
	x3max =  X3MAX;
	return x3max;
}

float fx3min(){
	float x3min;
	x3min =  X3MIN;
	return x3min;
}

// 1 fonction qui liste les informations nécessaires pour les assolements
int CreateAssolements(float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]){
	int i,i1,i2,i3,i4,i5,j;
	i = 0;
	//printf("%d\n ",i);

	for (i1=0;i1<NB_POSSIBLE_CROPS-4;i1++){
		for (i2=i1+1;i2<NB_POSSIBLE_CROPS-3;i2++){
			for (i3=i2+1;i3<NB_POSSIBLE_CROPS-2;i3++){
				for (i4=i3+1;i4<NB_POSSIBLE_CROPS-1;i4++){
					for (i5=i4+1;i5<NB_POSSIBLE_CROPS;i5++){
						for (j=0;j<6;j++){
							//printf("%d %d\n ",i,j);
							Assolements[i][5*j] = POSSIBLE_CROPS_PARAM[i1][j];
							Assolements[i][5*j+1] = POSSIBLE_CROPS_PARAM[i2][j];
							Assolements[i][5*j+2] = POSSIBLE_CROPS_PARAM[i3][j];
							Assolements[i][5*j+3] = POSSIBLE_CROPS_PARAM[i4][j];
							Assolements[i][5*j+4] = POSSIBLE_CROPS_PARAM[i5][j];
						}
						for (j=6;j<18;j++){
							Assolements[i][j+24] = (POSSIBLE_CROPS_PARAM[i1][j] + POSSIBLE_CROPS_PARAM[i2][j] + POSSIBLE_CROPS_PARAM[i3][j] + POSSIBLE_CROPS_PARAM[i4][j] + POSSIBLE_CROPS_PARAM[i5][j])/NB_CROPS;
						}
						i = i+1;
					}
				}
			}

		}

	}
	return 0;
}

//4 Fonctions qui "calculent" les bornes du domaine considéré pour les valeurs des 2 controles : u2 et u3

float fu2max(){
	float u2max;
	u2max =  U2MAX;
	return u2max;
}

float fu2min(){
	float u2min;
	u2min =  U2MIN;
	return u2min;
}

float fu3max(){
	float u3max;
	u3max =  U3MAX;
	return u3max;
}

float fu3min(){
	float u3min;
	u3min =  U3MIN;
	return u3min;
}


#include <math.h>
#include <stdio.h>
#include "dynamics.h"
#include "utils.h"


//ECONOMIC OUTCOMES (x1)


//Fonction qui calcule l'évolution du capital du restaurant

float GainRepas(float x2,float u3){
    return NB_CLIENTS_MAX*x2*u3*GAIN;
}

float CoutAchatFoodExterieur(float x2,float Rn_x3_u1){
	float c;
	float food_quant;
	///Computes expenses linked to food bought from other producers.
	food_quant = NB_CLIENTS_MAX*x2*FPP;
	c = fmaxf(food_quant-Rn_x3_u1,0)*FOOD_PRICE;
	return c;
}

float Capital_evolutionR(float x2,float u3,float Rn_x3_u1){
    float food_quant;
    ///Computes money earnings once meals have been sold
    food_quant = NB_CLIENTS_MAX*x2*FPP;
    return NB_CLIENTS_MAX*x2*u3*GAIN-fmaxf(food_quant-Rn_x3_u1,0)*FOOD_PRICE-FIXED_COSTSR;
}


//Fonction qui calcule l'évolution du capital de l activité agricole


float Cout(int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2){
    int i;
    float expenses=0.0;
    for (i=NB_CROPS;i<2*NB_CROPS;i++){
        expenses = expenses +Assolements[u1][i];
    }
    expenses = expenses*10000 ; // Passage d'€/m² en €/ha
    return expenses* u2 / NB_CROPS + FIXED_COSTSA;
}

float Capital_evolutionA(float Rn_x3_u1,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2){
    /// Unused !
    float expenses=0.0;
    expenses = Cout(u1,Assolements,u2);
    return Rn_x3_u1*PRICE_MTX-expenses-FIXED_COSTSA;
}


// Gains hotellerie (GH)

float GainH(float x5, float x6, float u4, float u5, float u6){
    int P;
    P = 2*u4*PRICE_TOURIST;
    return (GAINH-u5-u6)*x5*P+u6*x5*PRICE_GUEST-FIXED_COSTSH*x6/(x6+PHI_G);
    }



//ATTRACTIVENESS (x2)

// Fonction qui calcule d'évolution du coefficent d'attractivité


float Attractiveness_evolution(float x2,float u3,float Rn_x3_u1){
    float x2next,newx2,r;

    r = Rn_x3_u1/(NB_CLIENTS_MAX*FPP*x2)-R_MID;
    newx2 = x2 * (MU_P*u3/(u3+PHI_P)+MU_R*r/(r+PHI_R)+1);

    x2next = fmaxf(X2MIN, fminf(X2MAX,newx2));
    return x2next;
}




//FOOD PRODUCTION (R)


float R_n(float x3,float RM,float ri){
    /// fonction qui donne la production pour une espece caractérisée par TM et ri et une qualité du sol X3 en kg/m2
        float Rn;
        if (x3==0.0) Rn = 0.0;
        else if (ri!=0.5){
            if (x3<0.5){

                Rn = 2*RM*(x3*(1-2*ri)+ri-sqrtf(ri*ri+2*x3*(1-2*ri)))/(2*ri-1);
            }
            else {
                Rn = RM*(1+2*ri*((1-2*ri)*(x3-0.5)-(1-ri)+sqrtf((1-ri)*(1-ri)+2*(x3-0.5)*(2*ri-1)))/(2*ri-1));

            }

       if(Rn<0){
       	printf("Rn négatif (1)\n");
       	printf("Rm x3 ri Rn %f %f %f %f\n",RM,x3,ri,Rn);

       	getchar();
       }
	else if(isnan(Rn)){
       	printf("Rn nan (1)");
       	printf("Rm ri Rn %f %f %f %f\n",RM,ri,Rn,ri*ri+2*x3*(1-2*ri));

       	getchar();
       }

        }
        else {
            if (x3<0.5){
                Rn = 2*RM*x3;
            }
            else{
                Rn = RM*(x3+0.5);
           }
       if(Rn<0){
       	printf("Rn négatif (2)");
       	printf("Rm ri %f %f\n",RM,ri);
       	getchar();
       }

       }
       return Rn;
}



float R_nS(float x3, int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2){
    ///fonction qui donne la production d'une rotation de nb_crops especes distinctes caractérisées chacune par TM et ri et une qualité du sol x3 et la surface u2
    ///u2 en hectares, R_nS en kg
    int i,j;
    float rwnum,rwden; // relative_work numerateur / dénominateur
    float RnS=0;
    for (i=0;i<NB_CROPS;i++){
            RnS = RnS+R_n(x3,Assolements[u1][i],Assolements[u1][i+2*NB_CROPS]);
    }
    RnS = RnS*10000;//passage m2 à hectare
    RnS = RnS*u2/NB_CROPS;
    rwnum = 0;
    rwden = 0;
    for (j=0;j<12;j++){
            rwnum = rwnum+fminf(THR_WORKLOAD,Assolements[u1][6*NB_CROPS+j]*u2);
            rwden = rwden+Assolements[u1][6*NB_CROPS+j]*u2;
    }
    if (rwden>0) {
	    RnS = RnS*rwnum/rwden;
    }

    return RnS*(1-WASTE);
}


// SOIL QUALITY (x3)

float BISQ_evol(float x3, int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float x3min,float x3max){
    /// fonction qui calcule l'évolution de l'indice de qualité du sol
	float dIb = 0;
	float dIp = 0;
	float yn;
	int i;
	for (i=0;i<NB_CROPS;i++){
        yn = R_n(x3,Assolements[u1][i],Assolements[u1][2*NB_CROPS+i]);
		dIb = dIb + yn/(2*Assolements[u1][i])*Assolements[u1][3*NB_CROPS+i]*u2 / NB_CROPS;
		dIp = dIp + (x3 * Assolements[u1][4*NB_CROPS+i] + Assolements[u1][5*NB_CROPS+i])*u2/NB_CROPS;
	}
	return fminf(fmaxf(x3 - dIb + dIp+RAP_FALLOW*(U2MAX-u2),x3min),x3max);
}



// ENVIRONMENTAL QUALITY (x4)

float Env_evol(float x4, float GH, float TH, float S){
    return x4*(GROWTHRATE*x4*(1-x4/CARRYING_CAP)-GAMMA*TH+SIGMA*S+1);
    }



// TOURISTS (x5)

float Tourists_evol(float x5, float x6, float u4, float E){
    return x5*(MU_E*E/(E+PHI_E)+MU_C*x6/(x6+PHI_C*x5+PHI_C)-ALPHA*x5-u4+1);
    }



// TOURISTIC INFRASTRCTURES (x6)

float Infrastructures_evol(float x5, float x6, float u4, float u5){
    return (1-DELTA)*x6+u5*x5*2*u4*PRICE_TOURIST;
    }


// SOCIAL VALUE (x7)

float Socio_evol(float x7, float T, float F, float P){
    return x7+(MU*F/T - NU * (P-PRICE_FARE)/(U3MAX-U3MIN));
               }


//fonction qui calcule une borne supérieure de la production maximale de l activité agricole

float production_maximale(){
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
	float rcurrent,rres;
	int i,j,b;
	b = CreateAssolements(Assolements);
	rres = 0;
	for(i=0;i<NB_ASSOLEMENTS;i++) {
		for(j=0;j<DU2;j++) {
			rcurrent = R_nS(X3MAX,i,Assolements,Valeur(j,U2MAX,U2MIN,DU2));
			//printf("i %d j %d u2 %f %f\n",i,j,Valeur(j,U2MAX,U2MIN,DU2),rcurrent);
			if (rres < rcurrent) rres = rcurrent;
		}
	}
	return rres;
}

float cout_maximal(){
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
	float ecurrent,eres;
	int i,j,b;
	b = CreateAssolements(Assolements);
	eres = 0;
	for(i=0;i<NB_ASSOLEMENTS;i++) {
		for(j=0;j<DU2;j++) {
			ecurrent = Cout(i,Assolements,Valeur(j,U2MAX,U2MIN,DU2));
			if (eres < ecurrent) eres = ecurrent;
		}
	}
	return eres;
}


float gain_maximal(float Rmax){
	float ecurrent,eres,x2min,x2max,x2,u3,u3min,u3max;
	int i,j;
	eres = 0;

	x2min = fx2min();
	x2max= fx2max();
    u3min = fu3min();
	u3max = fu3max();

	for(i=0;i<DX2;i++) {
            x2 = Valeur(i,x2max,x2min,DX2);
		for(j=0;j<DU3;j++) {
            u3 = Valeur(j,u3max,u3min,DX3);
			ecurrent = GainRepas(x2,u3)-CoutAchatFoodExterieur(x2,Rmax)-FIXED_COSTSR;
			if (eres < ecurrent) eres = ecurrent;
		}
	}
	return eres;
}


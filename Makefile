prog: main.o organisation3.o dynamics.o affichage.o utils.o init.o
	gcc -o prog main.o organisation3.o dynamics.o affichage.o utils.o init.o -lm

main.o: main.c
	gcc -c -Wall main.c

organisation3.o: organisation3.c
	gcc -c -Wall organisation3.c

dynamics.o: dynamics.c
	gcc -c -Wall dynamics.c

affichage.o: affichage.c
	gcc -c -Wall affichage.c

utils.o: utils.c
	gcc -c -Wall utils.c

init.o: init.c
	gcc -c -Wall init.c

#include <stdio.h>
#include "affichage.h"
#include "init.h"
#include "utils.h"
#include "dynamics.h"
#include "organisation3.h"
#include "affichage.h"

//Inscription dans un fichier
//Valeurs des paramètres

//Inscription préambule information sur les paramètres généraux
void InscriptionParametres(FILE *f){
//	fprintf(f,"Paramètres de la dynamique\n");
//        fprintf(f,"VERSION : %s\n",VERSION);
//        fprintf(f,"Paramètres de la dynamique\n");
//        fprintf(f,"Gain : %f\n",GAIN);
//        fprintf(f,"Fixed_costs_R : %d\n",FIXED_COSTSR);
//        fprintf(f,"Food_price : %f\n",FOOD_PRICE);
//        fprintf(f,"Price_Mtrx : %f\n",PRICE_MTX);
//        fprintf(f,"Fixed_costs_A : %d\n",FIXED_COSTSA);
//        fprintf(f,"Nb clients max : %d\n",NB_CLIENTS_MAX);
//        fprintf(f,"Food per plate : %f\n",FPP);
//        fprintf(f,"Workload threshold : %d\n",THR_WORKLOAD);
//        fprintf(f,"Waste : %f\n",WASTE);
//        fprintf(f,"Rmid : %f\n",R_MID);
//        fprintf(f,"\n");
//	fprintf(f,"Paramètres de la grille et de la discrétisation\n");
//	fprintf(f,"x1max %f ",fx1max());
//	fprintf(f,"x1min %f ",fx1min());
//	fprintf(f,"DX1 %d\n",DX1);
//	fprintf(f,"x2max %f ",fx2max());
//	fprintf(f,"x2min %f ",fx2min());
//	fprintf(f,"DX2 %d\n",DX2);
//	fprintf(f,"x3max %f ",fx3max());
//	fprintf(f,"x3min %f ",fx3min());
//	fprintf(f,"DX3 %d\n",DX3);
//	fprintf(f,"u2max %f ",fu2max());
//	fprintf(f,"u2min %f ",fu2min());
//	fprintf(f,"DU2 %d\n",DU2);
//	// Manque la description de l'ensemble des assolements
//	fprintf(f,"u3max %f ",fu3max());
//	fprintf(f,"u3min %f ",fu3min());
//	fprintf(f,"DU3 %d\n",DU3);
//	fprintf(f,"Rmax %f ",production_maximale());
//	fprintf(f,"DR %d \n",DR);
//	fprintf(f,"Emax %f ",cout_maximal());
//	fprintf(f,"DE %d \n",DE);
//	fprintf(f,"\n");
}



void O3_InscriptionResultats(FILE *f, float surfaces_Agricole2D[DR][DE], float surfaces_Restaurant2D[DR][DE], float surfaces_Val[DR][DE], float surfaces_produit[DR][DE]){
    float Rmin, Emin;
    int i, j;

    float Rmax = production_maximale();
	float Emax = cout_maximal();

//    for (i=0;i<O3_DR;i++){
//		if (i==0) fprintf(f,"[");
//		else fprintf(f,",");
//		Rmin = Valeur(i,RMAX,0.0,O3_DR);
//		fprintf(f,"%f",Rmin);
//		if (i==O3_DR-1) fprintf(f,"]\n");
//	}
//	fprintf(f,"\n");
//	for (i=0;i<O3_DE;i++){
//		if (i==0) fprintf(f,"[");
//		else fprintf(f,",");
//		Emin = Valeur(i,EMAX,0.0,O3_DE);
//		fprintf(f,"%f",Emin);
//		if (i==O3_DE-1) fprintf(f,"]\n");
//	}
//	fprintf(f,"\n RESTAURANT\n");
//	for (i=0;i<O3_DE;i++){
//		if (i==0) fprintf(f,"[");
//		else fprintf(f,",");
//		for (j=0;j<O3_DR;j++){
//			if (j==0) fprintf(f,"[");
//			else fprintf(f,",");
//			fprintf(f,"%f",surfaces_Restaurant2D[j][i]);
//			if (j==O3_DR-1) fprintf(f,"]");
//		}
//		if (i==O3_DE-1) fprintf(f,"]\n");
//	}
//
//	fprintf(f,"\n AGRICULTURE\n");
//	for (i=0;i<O3_DE;i++){
//		if (i==0) fprintf(f,"[");
//		else fprintf(f,",");
//		for (j=0;j<O3_DR;j++){
//			if (j==0) fprintf(f,"[");
//			else fprintf(f,",");
//			fprintf(f,"%f",surfaces_Agricole2D[j][i]);
//			if (j==O3_DR-1) fprintf(f,"]");
//		}
//		if (i==O3_DE-1) fprintf(f,"]\n");
//    }

    fprintf(f,"\n PRODUIT\n");
	for (i=0;i<DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",surfaces_produit[j][i]);
			if (j==DR-1) fprintf(f,"]");
		}
		if (i==DE-1) fprintf(f,"]\n");
    }

        fprintf(f,"\n VALUES\n");
	for (i=0;i<DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",surfaces_Val[j][i]);
			if (j==DR-1) fprintf(f,"]");
		}
		if (i==DE-1) fprintf(f,"]\n");
    }

    fprintf(f,"\n");
    fprintf(f,"\n");
    fprintf(f,"\n");
}


void Affichage_vecteur(FILE *f,float xmax,float xmin,int dx){
	int i;
	fprintf(f,"[");
	for (i=0;i<dx;i++){
		fprintf(f,"%f",Valeur(i,xmax,xmin,dx));
		if (i < dx-1) fprintf(f,",");
	}
	fprintf(f,"]\n");
}

void Affichage_Noyau_1D_x3(FILE *f,int Values[DX3]){
	int i;
	fprintf(f,"[");
	for (i=0;i<DX3;i++){
		fprintf(f,"%d",Values[i]);
		if (i < DX3-1) fprintf(f,",");
	}
	fprintf(f,"]\n");
}

void Affichage_Frontiere_Noyau_2D_x2x1(FILE *f,int Values[DX2]){
	int i;
	fprintf(f,"[");
	for (i=0;i<DX2;i++){
		fprintf(f,"%d",Values[i]);
		if (i < DX2-1) fprintf(f,",");
	}
	fprintf(f,"]\n");
}

void Affichage_Noyau_1D_x2(FILE *f,int Values[DX2]){
	int i;
	fprintf(f,"[");
	for (i=0;i<DX2;i++){
		fprintf(f,"%d",Values[i]);
		if (i < DX2-1) fprintf(f,",");
	}
	fprintf(f,"]\n");
}

void Affichage_Frontiere_Noyau_2D_x3x1(FILE *f,int Values[DX3]){
	int i;
	fprintf(f,"[");
	for (i=0;i<DX3;i++){
		fprintf(f,"%d",Values[i]);
		if (i < DX3-1) fprintf(f,",");
	}
	fprintf(f,"]\n");
}

void Affichage_Frontiere_Noyau_3D_x4x1x7(FILE *f, int DX4, int DX7, int Values[DX4][DX7]){
	int i,j;
	fprintf(f,"[");
	for (i=0;i<DX4;i++){
        fprintf(f,"[");
        for (j=0;j<DX7;j++){
            fprintf(f,"%d",Values[i][j]);
            if (j < DX7-1) fprintf(f,",");
            else fprintf(f,"]");
        }
		if (i < DX4-1) fprintf(f,",");
	}
	fprintf(f,"]\n");
}

void Affichage_Frontiere_Noyau_2D_x5x6(FILE *f,int Values[DX5][DX6]){
	int i,j;
	fprintf(f,"[");
	for (i=0;i<DX5;i++){
	    fprintf(f,"[");
            for(j=0;j<DX6;j++){
		fprintf(f,"%d",Values[i][j]);
		if (j < DX6-1) fprintf(f,",");
		else fprintf(f,"]");
            }
        if (i < DX5-1) fprintf(f,",");
	}
	fprintf(f,"]\n");
}

void Affichage_Frontiere_Produit(FILE*f, int ValuesX2[DX2], int ValuesX3[DX3]){
    int i, j, k;
	fprintf(f,"[");
	for (i=0;i<DX2;i++){
	    for (j=0;j<DX3;j++){
	        if (j==0) fprintf(f,"[");
 //           k = Indice(Valeur(ValuesX2[i],X1MAX,X1MIN,DX1)+Valeur(ValuesX3[j],X1MAX,X1MIN,DX1),X1MAX,X1MIN,DX1);
            k = ValuesX3[j];
		fprintf(f,"%d",k);
		if (j < DX3-1) fprintf(f,",");
        else fprintf(f,"]");
	}
	if (i<DX2-1) fprintf(f,",");
	}
	fprintf(f,"]\n");

}


void Affichage_Traj(FILE *f, float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX]){
    int i;

    fprintf(f, "T = %d\n", TMAX);

       fprintf(f,"traj_x1 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x1[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_x2 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x2[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_x3 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x3[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u1 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%d",traj_u1[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u2 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_u2[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u3 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_u3[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
}

void Affichage_Traj_i(FILE *f, float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX],int n){
    int i;

       fprintf(f,"traj_x1_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x1[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_x2_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x2[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_x3_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x3[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u1_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%d",traj_u1[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u2_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_u2[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u3_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_u3[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
}

//Sortie destinée à être copiée_collée dans le Notebook python

void Affichage_pour_affichage_python(FILE *f,int DX1, int DX4,int DX7,int Values_O3_x3x1[DX3],int Values_O3_x2x1[DX2],int Values_O3_x4x1[DX4][DX7], int Values_x5x6[DX5][DX6]){
	int i,j,k;
	fprintf(f,"# A récuperer du .c\n## concernant les grilles : Grille_x1 tableau de taille DX1; Grille_x2 tableau de taille DX2 ; Grille_x3 tableau de taille DX3...\n");
	fprintf(f,"Grille_x1 = ");
	Affichage_vecteur(f,X1MAX,X1MIN,DX1);
	fprintf(f,"Grille_x2 = ");
	Affichage_vecteur(f,X2MAX,X2MIN,DX2);
	fprintf(f,"Grille_x3 = ");
	Affichage_vecteur(f,X3MAX,X3MIN,DX3);
	fprintf(f,"Grille_x4 = ");
	Affichage_vecteur(f,X4MAX,X4MIN,DX4);
	fprintf(f,"Grille_x5 = ");
	Affichage_vecteur(f,X5MAX,X5MIN,DX5);
	fprintf(f,"Grille_x6 = ");
	Affichage_vecteur(f,X6MAX,X6MIN,DX6);
	fprintf(f,"Grille_x7 = ");
	Affichage_vecteur(f,X7MAX,X7MIN,DX7);

	fprintf(f,"\n#Le noyau Agricole 1D\n");
	fprintf(f,"O3_Noyau_x3x1 = ");
	Affichage_Frontiere_Noyau_2D_x3x1(f,Values_O3_x3x1);
	fprintf(f,"#Le noyau Restaurant 1D\n");
	fprintf(f,"O3_Frontiere_Noyau_x2x1 =");
	Affichage_Frontiere_Noyau_2D_x2x1(f,Values_O3_x2x1);
	fprintf(f,"#Le noyau Hotel 2D\n");
	fprintf(f,"Frontiere_Noyau_x5x6 =");
	Affichage_Frontiere_Noyau_2D_x5x6(f,Values_x5x6);
    fprintf(f,"#Le noyau O3 Valeurs 3D\n");
	fprintf(f,"O3_Frontiere_Noyau_x4x1 =");
	Affichage_Frontiere_Noyau_3D_x4x1x7(f, DX4, DX7,Values_O3_x4x1);

}



void SaveKernel(FILE *f,int DX4,int DX7,int ValuesU[DX4][DX7]){
   fwrite(ValuesU, sizeof(int), DX4*DX7, f);
}


void LoadKernel(FILE *f,int DX4,int DX7,int ValuesU[DX4][DX7]){
    fread(ValuesU, sizeof(int), DX4*DX7, f);
}


void SaveTimes(FILE *f, float comp_times[DR][DE]){

    float Rmin, Emin;
    int i, j;

    float Rmax = production_maximale();
	float Emax = cout_maximal();

	for (i=0;i<DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",comp_times[j][i]);
			if (j==DR-1) fprintf(f,"]");
		}
		if (i==DE-1) fprintf(f,"]\n");
	}
}

//void Affichage_Surfaces(FILE *f, float X4V, float Rmin, float EAmax, float ERmin, float GHmin, float Smin, float Tomax, float Pmax, float Fmin, int Intx1x3frontiere[DX3], int Intx2frontiere[DX2], int Intx5x6frontiere[DX5][DX6], int Intx4frontiere[DX4][DX7]){
//fprintf(f,"INPUTS : \n");
//fprintf(f,"X4V = %f \n", X4V);
//fprintf(f,"Rmin = %f \n", Rmin);
//fprintf(f,"EAmax = %f \n", EAmax);
//fprintf(f,"ERmin = %f \n", ERmin);
//fprintf(f,"GHmin = %f \n", GHmin);
//fprintf(f,"Smin = %f \n", Smin);
//fprintf(f,"Tomax = %f \n", Tomax);
//fprintf(f,"Pmax = %f \n", Pmax);
//fprintf(f,"Fmin = %f \n\n", Fmin);
//
//fprintf(f,"OUTPUTS : \n");
////fprintf(f,"Volume agricole = %f \n", O3_CalculSurfacenoyauAgricole2D(Intx1x3frontiere));
////fprintf(f,"Volume restaurant = %f \n", O3_CalculSurfacenoyauRestaurant2D(Intx2frontiere));
////fprintf(f,"Volume hotel = %f \n", O3_CalculSurfacenoyauHotel2D(Intx5x6frontiere));
////fprintf(f,"Volume valeurs = %f \n", O3_CalculSurfacenoyauVal3D(Intx4frontiere));
//
//fprintf(f,"surf_table = [%f,%f,%f,%f]", O3_CalculSurfacenoyauAgricole2D(Intx1x3frontiere), O3_CalculSurfacenoyauRestaurant2D(Intx2frontiere), O3_CalculSurfacenoyauHotel2D(Intx5x6frontiere), O3_CalculSurfacenoyauVal3D(Intx4frontiere));
//}

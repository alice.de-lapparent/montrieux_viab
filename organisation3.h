#ifndef _ORGANISATION3_H_
#define _ORGANISATION3_H_

#include "init.h"
#include "dynamics.h"

//Pour l'�valuation de l'organisation 1 en fonction des diff�rentes valeurs des engagements
#define DR 20 //60
//#define DG 20
#define DE 20 //40
//#define DS 20
//#define DTo 20
//#define DP 20
//#define DF 20

#define RMAX 55000
#define GMAX 150000
#define SMAX 1.0
#define EMAX 1.0
#define ToMAX 11000
// #define PMAX 50 //On utilise U3MAX
#define FMAX 1000

#define GMIN 0
#define EMIN 0.0
#define ToMIN 1
//#define PMIN 1 //On utilise U3MIN
#define FMIN 0
#define SMIN 0.0

// AGRICULTURE
// Calcul du noyau de viabilit� garanti de l'agricole 1 dimension d'�tat x3, deux de contr�les u1 et u2,  une contrainte plus deux engagements.

// La contrainte verifie que x3 dans l'ensemble de contraintes (ou autrement dit de satisfaction)*****************************************

int IsinConstraintSetAgricole2D(float x3, float Smin);

// Les deux engagements sous forme de limitations de l'ensemble des controles admissibles
// fonction qui verifie si la paire de controles (u1,u2) est admissible lorsque l'�tat du syst�me est x3 �tant donn� les deux engagements pris *****************************************

int IsinAdmissibleControlSetAgricole2D(float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float Rmin, float EAmax, float Smin);

//La dynamique de x3 :
float fx3next(float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float x3min,float x3max);

void O3_CalculNoyauAgricole2D(int Intx1x3frontiere[DX3],float Rmin,float EAmax,float ERmin, float Smin);


// RESTAURANT
// Calcul du noyau de viabilit� garanti du restaurant 2 dimension d'�tat x1 et x2, 1 contr�le u3,  une contrainte, PAS engagements.

// La contrainte verifie que (x1,x2) dans l'esnemble de contraintes (ou autrement dit de satisfaction)*****************************************

int IsinConstraintSetREstaurant2D(float x2);

// PAS d'engagements sous forme de limitations de l'ensemble des controles admissibles

int IsinAdmissibleControlSetRestaurant2D(float x2,float u3, float R, float Emin, float Pmax);

float fx2next(float x2,float u3,float Rn_x3_u1);


//Calcul de la fronti�re inf�rieure du noyau de viabilit� garanti comme tableau de valeurs Intx2frontiere[DX2] qui associe � des valeurs de l'attractivit�  allant de X2MIN � X2MAX l'indice de la valeur minimale du capital x1 appartenant au noyau de viabilit� garanti


void O3_CalculFrontiereNoyauRestaurant2D(int Intx1frontiere[DX2],float Rmin,float Emin, float Pmax);


float O3_fx1next(float x1,float Er,float Ea, float Eh);

float O3_fx4next(float x4,float S, float Eh, float Th);



// HOTEL

int IsinConstraintSetHotel2D(float x5, float x6, float Tomax);

int IsinAdmissibleControlSetHotel2D(float x5, float x6,float u4, float u5, float u6, float GHmin, float Fmin);

float fx5next(float x5,float x6, float u4, float u5, float E);

float fx6next(float x5, float x6,float u4, float u5);

void CalculNoyauHotel2D(int Intx5x6frontiere[DX5][DX6],float GHmin,float Tomax, float Fmin, float X4V);



// GLOBAL

// La contrainte verifie que (x1,x2) dans l'esnemble de contraintes (ou autrement dit de satisfaction)*****************************************

int O3_IsinConstraintSetValues(float x1,float x4, float x7, float X4V);



int O3_IsinAdmissibleControlSetValues(float x1,float x4, float x7, float Er, float Ea, float S, float Eh, float Th, float F, float P, float X4V);




//Calcul de la fronti�re inf�rieure du noyau de viabilit� garanti comme tableau de valeurs Intx2frontiere[DX2] qui associe � des valeurs de l'attractivit�  allant de X2MIN � X2MAX l'indice de la valeur minimale du capital x1 appartenant au noyau de viabilit� garanti


void O3_CalculFrontiereNoyauValues(int DX1, int DX4, int DX7, int DT,int Intx1frontiere[DX4][DX7],float Smin,float EAmax,float ERmin,float EHmin,float Tomax, float Fmin, float Pmax, float X4V);

//Calcul du pourcentage d'occupation du noyau
float O3_CalculSurfacenoyauAgricole2D(int Intx1x3frontiere[DX3]);

float O3_CalculSurfacenoyauRestaurant2D(int Intx2frontiere[DX2]);

float O3_CalculSurfacenoyauHotel2D(int Intx5x6frontiere[DX5][DX6]);

float O3_CalculSurfacenoyauVal3D(int DX1, int DX4, int DX7,int Intx4frontiere[DX4][DX7]);

//float O3_CalculSurfacenoyauProduit(int Intx1x3frontiere[DX3], int Intx2frontiere[DX2]);




#endif


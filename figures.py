# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 16:18:23 2024

@author: adelapparent
"""


import os
import pandas as pd
import matplotlib.pyplot as plt



nb_indiv = 2500
nb_gen = 12

output_file_name = "sizes_output.txt"

size_gen0 = 100
size_gen = 200

random_seed = 42



### Extraction

def RecupFit_i(i):
    
    with open(output_file_name, "r") as fp :
        lines = fp.readlines()
        
    # parsing TODO; error control
    line_index = 0
    while not lines[line_index].startswith("surf_table") :
        line_index += 1
        
    # this is quick and dirty, maybe a regex would be better
    tokens_1 = lines[line_index][:-2].split("[")
    tokens_2 = tokens_1[1].split(",")
    
    fitness_values = []
    for i in range(0, len(tokens_2)) :
        fitness_values.append(float(tokens_2[i]))
        
    
    if min(fitness_values) == 0 :
        fitness_values.append(0)
    
    else:
        line_index += 1
        while not lines[line_index].startswith("surf_V") :
            line_index += 1
            
        # this is quick and dirty, maybe a regex would be better
        tokens_3 = lines[line_index].split("=")
        fitness_values.append(float(tokens_3[1]))
        
    return fitness_values


def CompFit_i(fit_vect):
    
    fitness = [min(fit_vect), sum(fit_vect), fit_vect[3]]
    
    with open("output.txt", "r") as fp :
        lines = fp.readlines()
    
    line_index = len(lines)-1
    while not lines[line_index].startswith("Execution time") :
        line_index += 1
    
    tokens_3 = lines[line_index][:-2].split(":")
    fitness.append(float(tokens_3[1]))
    
    return fitness
    


def PopSelect(gen):
    data = pd.read_csv("unique-name/"+str(random_seed)+"-population.csv-generation-"+str(gen)+".csv")
    return data

    
#%%## Figures

def time_scat(data, gen, fit):
    dataplot = data.loc[data['generation']==gen]
    ax[fit-1].scatter(dataplot["Time"], dataplot.iloc[:,fit] )  
    ax[fit-1].set_ylabel("Fitness value "+str(fit))
    
    
    
def gen_scat(data, fit):
    ax[fit-1].scatter(data['generation'], data.iloc[:,fit])
    ax[fit-1].set_ylabel("Fitness value "+str(fit))
    

def gen_scat_select(data, fit):
    ax[fit-1].scatter(data['generation'],[i[fit-1] for i in data["fitness_value"]])
    ax[fit-1].set_ylabel("Fitness value "+str(fit))
    

#%%#### Main ######

fitness_table = []

gen = 0

for i in range(nb_indiv):
    
    if i == size_gen0 + gen*size_gen:
        gen += 1
    
    folder_name = "individual_%d" %i
    os.makedirs(folder_name, exist_ok=True)
    
    # change the working directory (only affects this process)
    os.chdir(folder_name)
    
    fitness_values = RecupFit_i(i)
    
    fitness_table.append([gen]+CompFit_i(fitness_values))
    
    os.chdir("..")
    

data = pd.DataFrame(fitness_table, columns = ["generation", "Fit1", "Fit2", "Fit3", "Time"])
    
#%%

data_select = PopSelect(0)

for i in range(1,nb_gen+1):
    data_select = pd.concat([data_select, PopSelect(i)])

data_select["fitness_value"]=data_select["fitness_value"].apply(eval)


#%%
gen = 12


fig,ax = plt.subplots(3, sharex=True,figsize=(8, 10))
for fit in range(1,4):
    time_scat(data, gen, fit)

fig.supxlabel("Time (s)")
plt.xscale('log')
plt.show()


## Pour toutes les générations

fig,ax = plt.subplots(3, sharex=True,figsize=(8, 10))
for gen in range(nb_gen+1):
    for fit in range(1,4):
        scat=ax[fit-1].scatter(data["Time"], data.iloc[:,fit], c=data["generation"])  
        ax[fit-1].set_ylabel("Fitness value "+str(fit))
        
        #ax[fit-1].legend()
legend1 = ax[0].legend(*scat.legend_elements(),loc='upper right', ncol=2, bbox_to_anchor=(1.05, 1.05))
ax[0].add_artist(legend1)
fig.supxlabel("Time (s)")
plt.xscale('log')
plt.show()

    

#%%

## Dans la pop totale

fig,ax = plt.subplots(3, sharex=True,figsize=(6, 6))
for fit in range(1,4):
    ax[fit-1].scatter(data['generation'], data.iloc[:,fit])
    ax[fit-1].set_ylabel("Fitness value "+str(fit))

fig.supxlabel("Generation")
fig.suptitle("Whole population")
plt.show()




## Parmi les individus sélectionnés

fig,ax = plt.subplots(3, sharex=True,figsize=(6, 6))
for fit in range(1,4):
    ax[fit-1].scatter(data_select['generation'],[i[fit-1] for i in data_select["fitness_value"]])
    ax[fit-1].set_ylabel("Fitness value "+str(fit))

fig.supxlabel("Generation")
fig.suptitle("Selected subpopulations")
plt.show()

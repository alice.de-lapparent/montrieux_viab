#ifndef _dynamics_H_
#define _dynamics_H_

#include "init.h"
#include "organisation3.h"

//Paramètres des dynamiques
//Pour x1
#define GAIN 0.7 // share of the raw income that do not include variable costs (excluding fixed costs)
#define FIXED_COSTSR 5000*12 // includes cookers salaries and equipment depreciation  # initial value = 500*365
#define FOOD_PRICE 1.5 // mean price in € of 1 kg of wholesale food  # initial value = 4
#define PRICE_MTX 2.0 // mean price at which the restaurant buys food from local production

#define FIXED_COSTSA 8000 + 2000*12

#define PRICE_TOURIST 150
#define PRICE_GUEST 0
#define GAINH 0.8
#define FIXED_COSTSH 50000 //€
#define PHI_G 50000

//Pour x2
#define NB_CLIENTS_MAX 10000 // 75*180 // maximal capacity of the number of meals served each year
#define FPP  0.8 // food per plate = the quantity of food in one plate (in kg)
#define MU_R 1
#define PHI_R 0.5
#define MU_P -1
#define PHI_P 30
#define R_MID 0.8 // Production potentielle relative, pour laquelle l'effet sur l'attractivité du restaurant est neutre.

// Pour x3
#define THR_WORKLOAD   4*35 //temps de travail max par mois (il y a une personne ici)
#define RAP_FALLOW 0.025
#define WASTE 0.05 // proportion of food waste between field and fork (storage issues...)

// Pour x4
#define GROWTHRATE 1
#define CARRYING_CAP 1
//#define BETA 1/GMAX
#define GAMMA 1/(2*ToMAX)
#define SIGMA 0.1

// Pour x5
#define MU_E 1
#define PHI_E 0.5
#define MU_C 1
#define PHI_C 100
#define ALPHA 0.0001

// Pour x6
#define DELTA 0.0375

// Pour x7
#define PRICE_FARE 15 // prix du menu considéré comme socialement équitable
#define MU 0.8
#define NU 0.1


// Paramètres pour les contraintes
//#define X3QUAL 0.2
#define X1V 0.0
//#define X4V 0.2
#define X7V 0.1

// Pour les trajectoires
#define TMAX 20


//Fonctions de calcul de la dynamique de x1

//Fonction qui calcule l'évolution du capital du restaurant

float GainRepas(float x2,float u3);
float CoutAchatFoodExterieur(float x2,float Rn_x3_u1);
float Capital_evolutionR(float x2,float u3,float Rn_x3_u1);

//Fonction qui calcule l'évolution du capital de l activité agricole

float Cout(int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2);
float Capital_evolutionA(float Rn_x3_u1,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2);

// Hotellerie

float GainH(float x5, float x6, float u4, float u5, float u6);

//Fonctions de calcul de la dynamique de x2

// Fonction qui calcule d'évolution du coefficent d'attractivité


float Attractiveness_evolution(float x2,float u3,float Rn_x3_u1);

//Fonctions de calcul de la dynamique de x3


//fonction qui donne la production pour une espece caractérisée par TM et ri et une qualité du sol X3 en kg/m2
float R_n(float x3,float RM,float ri);





//fonction qui donne la production d'une rotation de nb_crops especes distinctes caractérisées chacune par TM et ri et une qualité du sol x3 et la surface u2

float R_nS(float x3, int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2);



// fonction qui calcule l'évolution de l'indice de qualité du sol

float BISQ_evol(float x3, int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float x3min,float x3max);



// ENVIRONMENTAL QUALITY (x4)

float Env_evol(float x4, float GH, float TH, float S);



// TOURISTS (x5)

float Tourists_evol(float x5, float x6, float u4, float E);



// TOURISTIC INFRASTRCTURES (x6)

float Infrastructures_evol(float x5, float x6, float u4, float u5);


// SOCIAL VALUE (x7)

float Socio_evol(float x7, float T, float F, float P);




//fonction qui calcule une borne supérieure de la production maximale de l activité agricole

float production_maximale();

float cout_maximal();

float gain_maximal(float Rmax);

#endif


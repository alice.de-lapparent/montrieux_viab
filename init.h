#ifndef merdeINIT_H_
#define merdeINIT_H_





//Paramètres boites
#define X1MAX 100000.0 // €
#define X1MIN -2.0
#define X2MAX 1.0
#define X2MIN 0.0
#define X3MAX 1.0
#define X3MIN 0.0
#define X4MIN 0.0
#define X4MAX 1.0
#define X5MIN 0
#define X5MAX 11000
#define X6MIN 0
#define X6MAX 10000000 // €
#define X7MIN 0
#define X7MAX 1
#define NB_CROPS 5 // crops planted each year
#define NB_POSSIBLE_CROPS 9 // crops available
#define NB_ASSOLEMENTS 126 //9*8*7*6*5/5/4/3/2  // Ccrops vs possible
#define U2MAX 2.0
#define U2MIN 0.05
#define U3MAX 40.0
#define U3MIN 5.0
#define U4MIN 0.01
#define U4MAX 1
#define U5MIN 0
#define U5MAX 1
#define U6MIN 0
#define U6MAX 0.5

//Pour les grilles
//#define DX1 41 //151
#define DX2 51 //101
#define DX3 51 //201
//#define DX4 51
#define DX5 21
#define DX6 31
//#define DX7 21
#define DU2 21 //51
#define DU3 21 //31
#define DU4 21
#define DU5 11
#define DU6 11


/*const float Eggplant[] = {10.5,9.2,0.8, 0.2 ,0.12, 0,0,0,175,269,240,548,502,501,445,334,155,0};//[yield (kg/m²),expenses (EUR/m²),ri,rd,rp,rap,works[12]]
const float Carrot[] = {3, 0.3,0.65, 0.14 ,0.07, 0,0,0,0,0,0,27,10,135,161,13,1,212};
const float Cabbage[] = {1.3, 0.8,0.5, 0.21, 0.15,0,0,0,0,0,0,23,50,22,16,97,0,0};
const float Cucumber[] = {10, 5.3,0.8, 0.28, 0.15 ,0,0,0,178,419,848,918,70,0,0,0,0,0};
const float Squash[] = {3.25, 0.4,0.7, 0.17, 0.099, 0,0,0,0,16,40,4,5,5,115,0,0,0};
const float Greenbean[] = {2.25, 5.4,0.6, 0.09 ,0.01, 0.002,0,0,457,19,55,2075,2044,156,0,0,0,0};
const float Lettuce[] = {7.71, 1.7,0.6, 0.18, 0.12, 0,2,16,185,24,664,0,0,0,1,0,0,0};
const float Melon[] = {2, 1.1,0.7, 0.17, 0.1, 0,0,4,11,145,27,17,255,35,4,0,0,0};
const float Tomato[] = {13 ,5.2,0.8, 0.2, 0.13, 0.002,0,26,143,296,557,705,586,411,235,326,0,0};
*/

float ReadValue(FILE *f);

//*************************D abord les bornes de toutes les variables*********servira peut etre plus tard si on a envie de les changer ....
//4 Fonctions qui "calculent" les bornes du domaine considéré pour les valeurs des 3 états : x1, x2 et x3

float fx1max();
float fx1min();
float fx2max();
float fx2min();
float fx3max();
float fx3min();

// 1 fonction qui liste les informations nécessaires pour les assolements
int CreateAssolements(float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]);

//4 Fonctions qui "calculent" les bornes du domaine considéré pour les valeurs des 2 controles : u2 et u3

float fu2max();
float fu2min();
float fu3max();
float fu3min();

#endif
